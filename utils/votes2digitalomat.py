#!/usr/bin/env python3
# votes2digitalomat.py
#
# (c) 2024 Uli Fouquet <uli AT gnufix DOT de>
# License: AGPL 3.0 or later
#
# Turn howtheyvote.de and parltrack.eu data together with question templates
# into data for digital-o-mat.
#
import datetime
import json
import pathlib
import pprint


VOTE_IDS = [
        165993, 168444, 168424, 167712, 142263, 168264, 166183,
        157507, 137688, 156165, 156157 ]
VOTES_DB = "ep_votes.json"
POSITIONS = {
    "FOR": "for",
    "AGAINST": "against",
    "ABSTENTION": "abstained",
    "ABSENT": "absent",  # not used by howtheyvote
    "DID_NOT_VOTE": "didntvote",
}
POSITIONS_DICT = dict([(x, 0) for x in POSITIONS.values()])
PARLTRACK_POS = {"+": "for", "-": "against", "0": "abstained"}
PARTY_NAMES = [
    {
        "name": "cdu",
        "short_name": "CDU",
        "long_name": "Christlich Demokratische Union Deutschlands",
    },
    {
        "name": "spd",
        "short_name": "SPD",
        "long_name": "Sozialdemokratische Partei Deutschlands",
    },
    {"name": "gruene", "short_name": "Grüne", "long_name": "Bündnis 90/Die Grünen"},
    {"name": "linke", "short_name": "Linke", "long_name": "DIE LINKE."},
    {"name": "afd", "short_name": "AfD", "long_name": "Alternative für Deutschland"},
    {
        "name": "csu",
        "short_name": "CSU",
        "long_name": "Christlich-Soziale Union in Bayern e.V.",
    },
    {"name": "fdp", "short_name": "FDP", "long_name": "Freie Demokratische Partei"},
    {"name": "freie", "short_name": "Freie Wähler", "long_name": "Freie Wähler"},
    {
        "name": "piraten",
        "short_name": "Piraten",
        "long_name": "Piratenpartei Deutschland",
    },
    # {
    # 	"name": "npd",
    # 	"short_name": "NPD",
    # 	"long_name": "Nationaldemokratische Partei Deutschlands"
    # },
    #{
    #    "name": "lkr",
    #    "short_name": "LKR",
    #    #"long_name": "Liberal-Konservative Reformer"
    #    "long_name": "Liberal-Conservative Refomists"
    #},
    {
        "name": "oedp",
        "short_name": "ÖDP",
        "long_name": "Ökologisch-Demokratische Partei",
    },
    {"name": "partei", "short_name": "Die PARTEI", "long_name": "Die PARTEI"},
    # {
    # 	"name": "buendnis",
    # 	"short_name": "Bündnis C",
    # 	"long_name": "Bündnis C - Christen für Deutschland"
    # },
    #{"name": "buendnis", "short_name": "Bündnis D", "long_name": "Bündnis Deutschland"},
    {"name": "volt", "short_name": "Volt", "long_name": "Volt"},
    {
        "name": "familien",
        "short_name": "Familien",
        "long_name": "Familien-Partei Deutschlands",
    },
    {"name": "independent", "short_name": "Parteilos", "long_name": "Independent"},
]
# sort parties by 2019 German EP election results
# source: https://www.bundeswahlleiterin.de/europawahlen/2019/ergebnisse/bund-99.html#stimmentabelle10
results_de_2019 = {'CDU': 22.6, 'SPD': 15.8, 'Grüne': 20.5, 'Linke': 5.5, 'AfD': 11.0, 'CSU': 6.3,
                   'FDP': 5.4, 'Freie Wähler': 2.2, 'Piraten': 0.7, 'Tierschutzpartei': 1.4,
                   'NPD': 0.3, 'Familien': 0.7, 'ÖDP': 1.0, 'Die PARTEI': 2.4, 'Volksabstimmung': 0.2,
                   'BP': 0.2, 'DKP': 0.1, 'MLPD': 0.0, 'SGP': 0.0, 'Tierschutz hier!': 0.3,
                   'Tierschutzallianz': 0.2, 'Bündnis C': 0.2, 'BIG': 0.2, 'BGE': 0.1,
                   'Die Direkte!': 0.1, 'DiEM25': 0.3, 'III Weg': 0.0, 'Die Grauen': 0.2,
                   'Die Rechte': 0.1, 'Die Violetten': 0.1, 'Liebe': 0.1, 'Die Frauen': 0.1,
                   'Graue Panther': 0.2, 'LKR': 0.1, 'Menschliche Welt': 0.1, 'NL': 0.0, 'ÖkoLinX': 0.1,
                   'Die Humanisten': 0.2, 'Partei für die Tiere': 0.2, 'Gesundheitsforschung': 0.2,
                   'Volt': 0.7}
results_de_2019.update({"Parteilos": -0.1, "Bündnis D": 0.0})
replacements = {}

# Sort by election result in 2019, alphabetically afterwards
PARTY_NAMES = sorted(PARTY_NAMES, key=lambda p: (100 - results_de_2019[p["short_name"]], p["short_name"]))
PARTY_NAMES_MAP = dict(Germany=dict([(x["long_name"], x["name"]) for x in PARTY_NAMES]))


def get_list_chunks(fd):
    chunk = ''
    level = 0
    while True:
        rchunk = fd.read(32*1024*1024)
        if not rchunk:
            break
        for c in rchunk:
            if level:
                chunk += c
            if c == '{':
                level += 1
            elif c == '}':
                level -= 1
                if level == 0:
                    yield json.loads('{' + chunk)
                    chunk = ''

def read_vote_files(vote_ids):
    """Look for files `vote-<VOTE-ID>.json`

    that are supposed to hold parltrack data of single votes.
    """
    this_dir = pathlib.Path(__file__).parent
    result = []
    for vote_id in sorted(vote_ids):
        path = this_dir.joinpath("vote-%s.json" % vote_id)
        if path.is_file():
            with path.open() as fd:
                result.append(json.load(fd))
    return result


def read_parltrack_votes(vote_ids,db_file=VOTES_DB):
    # see, if all vote have already been extracted...
    already_written = read_vote_files(vote_ids)
    if len(already_written) == len(vote_ids):
        return already_written
    # ...otherwise we have to scan the big ep_votes.json
    result = []
    with open(db_file, "r") as fd:
        while fd.read(1) != '[':
            continue
        for n, vote in enumerate(get_list_chunks(fd)):
            if vote["voteid"] in vote_ids:
                with open("vote-%s.json" % vote["voteid"], "w") as fd:
                    json.dump(vote, fd, indent=2)
                result.append(vote)
            if not n % 1000:
                print(n)
    return result


def national_meps_by_party(
    meps, timestamp, country="Germany", party_names_map=PARTY_NAMES_MAP
):
    result = dict()
    invalid = []
    party_map = party_names_map[country]
    for mep in meps:
        for entry in mep["Constituencies"]:
            if entry is None:
                continue
            if entry["country"] != country:
                continue
            party = entry["party"]
            start = datetime.datetime.fromisoformat(entry["start"])
            end = datetime.datetime.fromisoformat(entry["end"])
            if end < timestamp or start > timestamp:
                continue
            if entry["party"] not in party_map.keys():
                invalid += ((mep, entry), )
                result[mep["UserID"]] = None
            else:
                result[mep["UserID"]] = party_names_map[country][entry["party"]]
            break
    for mep, entry in invalid:
        print(
            "WARNING: Voters party not in list of parties for next election:")
        pprint.pprint(dict(
            name=mep["Name"]["full"],
            userid=mep["UserID"],
            party=entry["party"])
            )
    return result, invalid


def get_parltrack_votes(vote_ids, all_meps):
    result = dict()
    for vote in read_parltrack_votes(vote_ids):
        vote_id = vote["voteid"]
        print(vote["voteid"])
        print(vote["title"])
        print(vote["url"])
        ts = datetime.datetime.fromisoformat(vote["ts"])
        uid_party_map, invalid = national_meps_by_party(meps, ts, "Germany")
        pprint.pprint(vote.keys())
        vote_list = {}
        mep_vote_dict = dict(
                [(mepid, dict(party=party, pos="didntvote"))
                    for (mepid, party) in uid_party_map.items()
                    if party is not None  # Ignore votes of invalid MEPs
                    ]
            )
        for party in set([v["party"] for v in mep_vote_dict.values()]):
            vote_list[party] = POSITIONS_DICT.copy()
        for (mepid, party, pos) in [
                (m["mepid"], uid_party_map[m["mepid"]], PARLTRACK_POS[pos])
                for (pos, plist) in vote["votes"].items()
                for g in plist["groups"].values()
                for m in g
                if m["mepid"] in uid_party_map.keys()
                ]:
            if mepid in mep_vote_dict.keys():
                mep_vote_dict[mepid]["pos"] = pos
        for mep_vote in mep_vote_dict.values():
            vote_list[mep_vote["party"]][mep_vote["pos"]] += 1
        result[str(vote_id)] = vote_list
    return result


this_dir = pathlib.Path(__file__).parent
print("Parse MEP data...")
with this_dir.joinpath("ep_meps.json").open() as fd:
    meps = json.load(fd)
print("Parse vote data....")
vote_dicts = get_parltrack_votes(VOTE_IDS, meps)

questions = []
for n, vote_dict in enumerate(vote_dicts.items()):
    vote_id, vote = vote_dict
    question_paths = list(pathlib.Path(this_dir).glob("question-%s-*.json" % vote_id))
    if len(question_paths) == 0:
        print("No question data found for vote %s. Filling up with sample data" % vote_id)
        path = this_dir / "sample_question.json"
    else:
        path = question_paths[0]
    qdata = json.load(open(str(path)))
    qdata["id"] = n + 1
    vlist = [dict(name=key, voting=dict(result=None, explanation=None, results=voting)) for key, voting in vote.items()]
    qdata["answers"] = vlist
    questions.append(qdata)

data = dict(questions=questions, parties=PARTY_NAMES)

with open("data.js", "w", encoding="utf-8") as fd:
    fd.write("var data = ")
    json.dump(data, fd, ensure_ascii=False, indent=4)
print("Result in: data.js")
print("Copy over to ../assets/data/data.js to see it in action")

