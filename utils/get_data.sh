#!/bin/bash
#
# Retrieve relevant data for digital-o-mat
#
# We download data about members of EU parliament and data about selected
# votes.
#
# Run like
#
#     $ source get_data.sh
#
# You must have `wget` and `lunzip` installed to run this script.


# Votes to download. The numbers must be valid vote ids on howtheyvote.eu. The
# trailing "-description-string" must start with a dash and can be set as you
# like.
VOTE_DATA=("142263-europol" "165993-eidas" "167712-chatcontrol-light" "168424-right-to-repair" "168444-ehds" "166183-media-freedom")

# Get memberdata...
if [ ! -f ep_meps.json ]; then
    if [ ! -f ep_meps.json.lz ]; then
        wget https://parltrack.eu/dumps/ep_meps.json.lz ;
    fi
    # unzip
    lunzip ep_meps.json.lz
fi

# Get parltrack vote data...
if [ ! -f ep_votes.json ]; then
    if [ ! -f ep_votes.json.lz ]; then
        wget https://parltrack.eu/dumps/ep_votes.json.lz ;
    fi
    lunzip ep_votes.json.lz
fi

# Get howtheyvote vote data...
for vote in "${VOTE_DATA[@]}"; do
    id=$(echo $vote | cut -d '-' -f 1)     # All before first hyphen...
    name=$(echo $vote | cut -d '-' -f 2-)  # ...all afterwards
    filename="vote-${id}-${name}.json"
    if [ ! -f $filename ]; then
        wget -O "${filename}" https://howtheyvote.eu/api/votes/${id}
    fi
done
