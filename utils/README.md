# Helpers for digital-o-mat

## tl;dr;

1) Change to this directory

2) Get the missing data:

    $ source get_data.sh

3) Generate data for digital-o-mat

    $ python3 votes2digitalomat.py


## Data Sources

### Members of European Parliament

We need the data of members of parliament to map national political parties to
vote results.

The MEP data can be retrieved from https://parltrack.eu/dumps
See https://parltrack.eu/schemas/ep_meps for a desription of the dataset
format.

### Vote Data

Vote data can be obtained from https://howtheyvote.eu.

Please see the local `get_data.sh` script for the list of votes we selected.

On howtheyvote.eu vote data can be retrieved in JSON format from

  `https://howtheyvote.eu/api/votes/<VOTE_ID>`

where `<VOTE_ID>` is a six-digit integer. Use the main site to look for
specific votes and their ids.


## `votes2digitalomat.py`

Parses the downloaded data and creates the numbers we need for digital-o-mat.
