var data = {
    "questions": [
        {
            "id": 1,
            "name": "Stalkerware-Werbung",
            "title": "Stalkerware-Werbung",
            "description": "Das Marketing für digitale Produkte, die Missbrauch erleichtern, insbesondere für Stalkerware ist besorgniserregend. Die Vorstellung von Stalkerware als „Apps für elterliche Kontrolle“ ist abzulehnen.",
            "terms": [
                {
                    "term": "Stalkerware ",
                    "explanation": "ist Software, meistens Smartphone-Apps, die der missbräuchlichen Kontrolle zumeist nahestehender Menschen, etwa Partner, Kinder oder Eltern dient. Stalkerware spioniert Menschen ohne deren Wissen oder Einverständnis aus und liefert z.B. Standortdaten, Chat-Mitschnitte oder Kontaktlisten der Ausspionierten. Viele dieser Apps stellen sich selbst als beziehungsfördernd oder familienfreundlich dar. Stalkerware ist eine Form der digitalen Gewalt oder „Cyberviolence“.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf mepwatch.eu",
                    "link": "https://mepwatch.eu/9/vote.html?v=137688&eugroup=&country=de",
                    "date": "13.12.2021"
                }
            ],
            "background": [
                {
                    "title": "Entschließungsantrag zu geschlechtsbezogener Cybergewalt:",
                    "link": "https://www.europarl.europa.eu/doceo/document/TA-9-2021-0489_DE.html",
                    "date": "14.12.2021"
                },
                {
                    "title": "Änderungsantrag zu Stalkerware (späterer § 47)",
                    "link": "https://www.europarl.europa.eu/doceo/document/A-9-2021-0338-AM-005-007_DE.pdf",
                    "date": "06.12.2021"
                },
                {
                    "title": "Stalkerware-Werbung erlaubt, wenn sie sich an Eltern richtet",
                    "link": "https://netzpolitik.org/2020/digitale-spionage-google-erlaubt-weiterhin-werbung-fuer-stalkerware/",
                    "date": "12.08.2020"
                },
                {
                    "title": "Stalking, Doxing, Nacktfotos: Was ist digitale Gewalt?",
                    "link": "https://netzpolitik.org/2024/stalking-doxing-nacktfotos-was-ist-digitale-gewalt/",
                    "date": "30.03.2024"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 14,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 10,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 2,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 4,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 6,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 21,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 22,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 2,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        },
        {
            "id": 2,
            "name": "Europol",
            "title": "Europol: Ausweitung der Befugnisse",
            "description": "Europol soll künftig auch Daten mit Unternehmen und nicht-europäischen Drittstaaten austauschen dürfen. Das bislang illegale Vorratsspeichern von Daten unschuldiger Dritter soll dabei nachträglich erlaubt werden.",
            "terms": [
                {
                    "term": "Europol ",
                    "explanation": "ist eine europäische Polizeibehörde ohne eigene Exekutivgewalt. Die Aufgabe dieser EU-Agentur ist die Unterstützung nationaler Polizeibehörden wie dem BKA bei der Bekämpfung internationalier Kriminalität vor allem durch Datenaustausch. In der Vergangenheit hatten EU-Staaten Europol gern zum Speichern beliebiger Daten, von Demonstrationsteilnehmenden-Daten über Passagierdaten bis hin zu biometrischen Überwachungsaufnahmen genutzt.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf howtheyvote",
                    "link": "https://howtheyvote.eu/votes/142263",
                    "date": "04.05.2022"
                }
            ],
            "background": [
                {
                    "title": null,
                    "link": "https://www.bpb.de/kurz-knapp/lexika/politiklexikon/17467/europol/",
                    "date": null
                },
                {
                    "title": null,
                    "link": "https://netzpolitik.org/2022/eu-parlament-stimmt-zu-neue-europol-verordnung-auf-der-zielgeraden/",
                    "date": null
                },
                {
                    "title": null,
                    "link": "https://netzpolitik.org/2022/klage-von-datenschuetzer-europols-riesige-datenberge-landen-vor-eu-gericht/",
                    "date": null
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 11,
                            "abstained": 3,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 7,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 2,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 5,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 18,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 3
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 22,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 4,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 2,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        },
        {
            "id": 3,
            "name": "Künstliche Intelligenz zur Bewertung einreisender Menschen",
            "title": "BÖHMERMANN SPEZIAL: KI-basierte Bewertung einreisender Menschen verbieten",
            "description": "Der Betrieb von KI-Systemen durch Asyl-, Migrations- oder Grenzbehörden, um Profile von Personen zu erstellen oder um zu beurteilen, ob eine Person, die in die EU einreisen will oder eingereist ist, eine Gefahr darstellen könnte, soll verboten werden.",
            "terms": [
                {
                    "term": "Künstliche Intelligenz ",
                    "explanation": "oder kurz „KI“ bezeichnet heutzutage zumeist die algorithmische Vorhersage von Sachverhalten basierend auf Wahrscheinlichkeiten, die aus großen Mengen von Eingabedaten gewonnen wurden. So können manche Systeme Gespräche simulieren, indem sie berechnen, was eine Person auf einen gegebenen Gesprächsbeitrag sehr wahrscheinlich antworten würde. KI ist bereits in vielen Alltagsgegenständen wie Smartphones und Autos verbaut. Sie kommt aber auch z.B. bei Waffensystemen, Kreditentscheidungen oder Überwachungs- und sozialen Kontrollsystemen zum Einsatz. Oft werden KI-basierte Entscheidungen über Menschen getroffen, ohne dass diese darüber informiert sind oder sich dagegen wehren können.",
                    "link": "https://de.wikipedia.org/wiki/K%C3%BCnstliche_Intelligenz"
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf mepwatch.eu",
                    "link": "https://mepwatch.eu/9/vote.html?v=156157&country=de",
                    "date": "14.06.2023"
                }
            ],
            "background": [
                {
                    "title": "ZDF Magazin Royale und Algorithm Watch: Wie die EU mit Künstlicher Intelligenz ihre Grenzen schützen will <https://fuckoffai.eu>",
                    "link": "https://fuckoffai.eu/",
                    "date": "24.05.2024"
                },
                {
                    "title": "Bericht A9-0188/2023 zum Gesetz über Künstliche Intelligenz",
                    "link": "https://www.europarl.europa.eu/doceo/document/A-9-2023-0188_DE.html",
                    "date": "22.05.2023"
                },
                {
                    "title": "Änderungsantrag 792 zu Bericht A9-0188/2023 zum Künstliche-Intelligenz-Gesetz",
                    "link": "https://www.europarl.europa.eu/doceo/document/A-9-2023-0188-AM-772-773_DE.pdf",
                    "date": "07.06.2023"
                },
                {
                    "title": "EU-Parlament macht Weg frei für KI-Verordnung",
                    "link": "https://netzpolitik.org/2024/trotz-biometrischer-ueberwachung-eu-parlament-macht-weg-frei-fuer-ki-verordnung/",
                    "date": "13.03.2024"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 14,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 8,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 2,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 18,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 3
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 2,
                            "against": 2,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 21,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 4,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        },
        {
            "id": 4,
            "name": "Künstliche Intelligenz zur Gefahrenabwehr erlauben",
            "title": "KI-basierte biometrische Erfassung von Menschen im öffentlichen Raum erlauben",
            "description": "In der EU soll es erlaubt werden können, zur Suche nach Vermissten, bei Gefahr von Terroranschlägen oder zur Identifizierung von Straftäter.innen Menschen mit KI-basierten biometrischen Systemen im öffentlichen Raum in Echtzeit zu erfassen.",
            "terms": [
                {
                    "term": "Künstliche Intelligenz ",
                    "explanation": "oder kurz „K.I.“ bezeichnet heutzutage zumeist die algorithmische Vorhersage von Sachverhalten basierend auf Wahrscheinlichkeiten, die aus großen Mengen von Eingabedaten gewonnen wurden. So können manche Systeme Gespräche simulieren, indem sie berechnen, was eine Person auf einen gegebenen Gesprächsbeitrag sehr wahrscheinlich antworten würde. K.I. ist bereits in vielen Alltagsgegenständen wie Smartphones und Autos verbaut. Sie kommt aber auch z.B. bei Waffensystemen, Kreditentscheidungen oder Überwachungs- und sozialen Kontrollsystemen zum Einsatz. Oft werden K.I.-basierte Entscheidungen über Menschen getroffen, ohne dass diese darüber informiert sind oder sich dagegen wehren können.",
                    "link": "https://de.wikipedia.org/wiki/K%C3%BCnstliche_Intelligenz"
                },
                {
                    "term": "Biometrische Erfassung ",
                    "explanation": "von Menschen bedeutet, dass sie aufgrund ihrer körperlichen Merkmale identifiziert werden. Diese Merkmale können z.B. das Gesicht, Fingerabdrücke oder die Gangart sein. Da diese Merkmale zumeist unveränderlich sind und zum Teil ohne Wissen der Betroffenen in der Öffentlichkeit „ausgelesen“ werden können, sind sie auf besonderen Schutz angewiesen.",
                    "link": null
                },
                {
                    "term": "Strafttat ",
                    "explanation": "im Sinne der Abstimmung sind solche Straftaten, deren Höchststrafe mindestens drei Jahre beträgt. In Deutschland fallen darunter auch Delikte wie Jagdwilderei, Betrug, so ziemlich alle Drogendelikte, Vollrausch oder Fahrerflucht.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf mepwatch.eu",
                    "link": "https://mepwatch.eu/9/vote.html?v=156165&country=de",
                    "date": "14.06.2023"
                }
            ],
            "background": [
                {
                    "title": "Bericht A9-0188/2023 zum Gesetz über Künstliche Intelligenz",
                    "link": "https://www.europarl.europa.eu/doceo/document/A-9-2023-0188_DE.html",
                    "date": "22.05.2023"
                },
                {
                    "title": "Änderungsantrag 801 zu Bericht A9-0188/2023 zum Künstliche-Intelligenz-Gesetz",
                    "link": "https://www.europarl.europa.eu/doceo/document/A-9-2023-0188-AM-800-801_DE.pdf",
                    "date": "07.06.2023"
                },
                {
                    "title": "EU-Parlament macht Weg frei für KI-Verordnung",
                    "link": "https://netzpolitik.org/2024/trotz-biometrischer-ueberwachung-eu-parlament-macht-weg-frei-fuer-ki-verordnung/",
                    "date": "13.03.2024"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 14,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 8,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 2,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 18,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 3
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 5,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 4,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 21,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 5,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 2,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        },
        {
            "id": 5,
            "name": "Transparenzpflichten für Abgeordnete im EP",
            "title": "Mehr Transparenz im EP",
            "description": "Alle MdEPs sowie ihre akkreditierten parlamentarischen Assistenten und Bediensteten sollen verpflichtet werden, alle geplanten Treffen mit Personen außerhalb des Parlaments zu veröffentlichen, wenn diese Treffen im Zusammenhang mit einem Bericht, einem Initiativbericht oder einer Entschließung des Europäischen Parlaments stehen;",
            "terms": [
                {
                    "term": "MdEP ",
                    "explanation": "steht für „Mitglied des Europäischen Parlaments“. Also alle gewählten Parlamentarierinnen und Parlamentarier.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf mepwatch.eu",
                    "link": "https://mepwatch.eu/9/vote.html?v=157507",
                    "date": "13.07.2023"
                }
            ],
            "background": [
                {
                    "title": "Text des Entschließungsantrags des Parlaments (§ 18, hinterer Teil stand zur Abstimmung)",
                    "link": "https://www.europarl.europa.eu/doceo/document/TA-9-2023-0295_DE.html",
                    "date": "13.07.2023"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 11,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 5
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 9,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 2,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 15,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 5
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 5,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 20,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 3
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        },
        {
            "id": 6,
            "name": "eIDAS-Reform",
            "title": "Einführung eines digitalen Identitätsnachweises",
            "description": "EU-Staaten sollen verpflichtet werden, ihren Bürgerinnen und Bürgern eine digitale „Brieftasche“ anzubieten. Mit dieser könnten diese sich gegenüber Behörden oder Web-Shops staatlich beglaubigt authentifizieren. Die Nutzung soll freiwillig sein und Identifizierungsvorgänge billiger und einfacher machen. Alle Transaktionsdaten sollen aufgezeichnet und zentral gespeichert werden.",
            "terms": [
                {
                    "term": "Digitale Brieftasche",
                    "explanation": " (oder englisch: „Wallet“) ist eine Software, die auf dem Smartphone oder einen Computer installiert wird und mithilfe von kryptographischen Verfahren einer Gegenstelle bestätigen kann, das bestimmte Daten zutreffend sind und der Staat dies beglaubigt. Solche bestätigten Daten können reale persönliche Angaben wie Name, Adresse oder Geburtsdatum sein oder auch pseudonymisierte Daten. Aber auch die einfache Angabe, ob eine Wallet-Inhaberin schon 18 ist oder nicht kann so elektronisch bestätigt werden (sog. „Zero Knowledge Proof“).",
                    "link": null
                },
                {
                    "term": "Zero Knowledge Proof ",
                    "explanation": " zu Deutsch: „kenntnisfreier Beweis“, ist ein Verfahren, mit dem eine Person A einer Stelle B nachweisen kann, dass sie eine bestimmte Eigenschaft hat, ohne diese Eigenschaft im Detail zu zeigen, z.B. ob sie schon 18 Jahre alt ist oder noch nicht. Die Gegenstelle erfährt dabei nur, ob die Person 18 oder älter ist, aber nicht, wie alt sie tatsächlich ist. EU-Staaten können selbst entscheiden, ob sie das Anbieten von kenntnisfreien Beweisen von Stellen verlangen oder nicht.",
                    "link": "https://de.wikipedia.org/wiki/Null-Wissen-Beweis"
                },
                {
                    "term": "Pseudonymisierung",
                    "explanation": " ersetzt reale Angaben, z.B. den Namen einer Person, durch ein Pseudonym. Häufig sind das zufällig generierte Zahlen- und Buchstabenkombinationen. Dies soll der Anonymisierung einer Person dienen. Je mehr reale Daten mit pseudonymen Daten verknüpft werden, z.B. Wohnort, Geburtsjahr oder bestimmte Krankheiten, desto eher wird die anonymisierende Wirkung von Pseudonymen aufgehoben und Personen sind dann dennoch eindeutig identifizierbar. EU-Staaten können selbst entscheiden, ob Behörden oder Geschäfte Pseudonymisierung anbieten müssen.",
                    "link": null
                },
                {
                    "term": "Transaktionsdaten",
                    "explanation": " sind hier Daten, wann wer welche Authentifizierungen bei wem und zu welchem Zweck vorgenommen hat. Die Daten sollen zwar logisch getrennt aufbewahrt werden, sind aber mit den entsprechenden staatlichen Berechtigungen wieder zusammenführbar.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf howtheyvote",
                    "link": "https://howtheyvote.eu/votes/165993",
                    "date": "29.02.2024"
                }
            ],
            "background": [
                {
                    "title": "netzpolitik.org zur eIDAS-Reform",
                    "link": "https://netzpolitik.org/2024/eidas-reform-eu-parlament-stimmt-fuer-digitale-brieftasche/",
                    "date": "29.02.2024"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 10,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 6
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 8,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 1,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 4,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 20,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 19,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 4
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 5,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 2,
                            "abstained": 1,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        },
        {
            "id": 7,
            "name": "Europäisches Medienfreiheitsgesetz",
            "title": "Europäisches Medienfreiheitsgesetz",
            "description": "Die Medien sollen durch das neue Gesetz besser geschützt werden. Spionagesoftware darf nur noch in Ausnahmefällen eingesetzt werden. Durch nationale Register sollen die Eigentumsverhältnisse von Medien transparenter werden. Medien sollen Auskünfte über staatliche Werbung und Finanzierung geben müssen. Große Online-Plattformen sollen nicht willkürlich Inhalte von Medienanbietern löschen dürfen.",
            "terms": [
                {
                    "term": "Spionagesoftware und ",
                    "explanation": "Spionagewerkzeuge wie Pegasus und Predator können unbemerkt auf Mobiltelefone aufgespielt werden. Dort können Sie auf Dateien Zugreifen und auch Chatnachrichten mitlesen. Diese Daten können dann über das Internet versendet werden. Der Einsatz von Spionagesoftware bei Journalist*Innen muss vorab durch eine Justizbehörde erlaubt werden, die wegen schwerer Straftaten ermittelt.",
                    "link": null
                },
                {
                    "term": "Transparenz der Eigentumsverhältnisse:",
                    "explanation": "In nationalen Registern sollen Informationen über die Eigentumsverhältnisse von Mediendiensteanbieter, die Nachrichten und Inhalte zum Zeitgeschehen bereitstellen, zur Verfügung gestellt werden. Insbesondere direkter oder indirekter staatlicher Besitz.",
                    "link": null
                },
                {
                    "term": "Schutz der Medienfreiheit in der EU vor großen Plattformen:",
                    "explanation": "Wenn eine große Online-Plattform wie Facebook, X (Twitter) oder Instagram unabhängige Medieninhalte willkürlich löschen oder einschränken möchte, so muss der betroffene Mediendiensteanbieter benachrichtigt werden und 24 Stunden Zeit zum Reagieren haben. Zudem soll durch das Medienfreiheitsgesetz eine außergerichtliche Streitbeilegungsstelle geschaffen werden, welche sich aus Medienaufsichtsbehörden bzw. -stellen der Mitgliedstaaten zusammensetzt.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf howtheyvote",
                    "link": "https://howtheyvote.eu/votes/166183",
                    "date": "13.03.2024"
                }
            ],
            "background": [
                {
                    "title": "Pressemitteilung der EU",
                    "link": "https://www.europarl.europa.eu/news/de/press-room/20240308IPR19014/medienfreiheitsgesetz-mehr-schutz-fur-journalisten-und-pressefreiheit-in-der-eu",
                    "date": "13.03.2024"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 11,
                            "against": 0,
                            "abstained": 4,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 9,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 1,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 6,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 17,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 3
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 21,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 5,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 1,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        },
        {
            "id": 8,
            "name": "Freiwillige Chatkontrolle",
            "title": "Freiwillige Chatkontrolle",
            "description": "Kommunikations-Anbieter sollen alle privaten Inhalte aller ihrer Kundinnen und Kunden in Chats, Emails oder Sprachnachrichten auf sexualisierte Gewalt an Kindern scannen dürfen. Dafür bedarf es einer Ausnahme von der ePrivacy Verordnung.",
            "terms": [
                {
                    "term": "Chatkontrolle",
                    "explanation": "Das automatisierte Scannen von privaten Nachrichten soll helfen, Hinweise über sexualisierte Gewalt an Kindern zu sammeln. Die Grundrechte der Vertraulichkeit der Kommunikation und dem Schutz personenbezogener Daten sollen dadurch eingeschränkt werden. Begründet wird dies durch die schwere Verletzung der Menschen- und Grundrechte durch sexualisierte Gewalt an Kindern. Einige Anbieter führten solche Scans bereits freiwillig durch. Die betroffenen Apps und Anwendungen waren bisher nicht von der ePrivacy Verordnung betroffen. Durch eine neue EU Richtlinie (2018/1972) sollten auch diese Dienste unter die ePrivacy Verordnung fallen und die Praxis der Scans drohte illegal zu werden. Damit das freiwillige Scannen auch zukünftig legal ist, wurde eine temporäre Verordnung erlassen, welche für solche Art von Scans eine Ausnahme von der ePrivacy Verordnung vorsieht. Diese wurde inzwischen verlängert.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf howtheyvote",
                    "link": "https://howtheyvote.eu/votes/167712",
                    "date": "10.04.2024"
                }
            ],
            "background": [
                {
                    "title": "EDRi: A beginner’s guide to EU rules on scanning private communications: Part 1",
                    "link": "https://edri.org/our-work/a-beginners-guide-to-eu-rules-on-scanning-private-communications-part-1/",
                    "date": null
                },
                {
                    "title": "netzpolitik.org: EU billigt Durchleuchtung privater Chats",
                    "link": "https://netzpolitik.org/2021/eprivacy-ausnahme-eu-billigt-durchleuchtung-privater-chats/",
                    "date": "30.04.2021"
                },
                {
                    "title": "netzpolitik.org: Verhältnismäßigkeit der freiwilligen Chatkontrolle fraglich",
                    "link": "https://netzpolitik.org/2023/bericht-eu-kommission-scheitert-verhaeltnismaessigkeit-der-freiwilligen-chatkontrolle-zu-belegen/",
                    "date": "20.12.2023"
                },
                {
                    "title": "Richtlinie (EU) 2018/1972",
                    "link": "https://eur-lex.europa.eu/eli/dir/2018/1972/oj?locale=de",
                    "date": "17.12.2018"
                },
                {
                    "title": "EU Ausnahme Verordnung",
                    "link": "https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A52020PC0568",
                    "date": "10.9.2020"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 12,
                            "against": 0,
                            "abstained": 2,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 6,
                            "absent": 0,
                            "didntvote": 3
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 1,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 5,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 6,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 20,
                            "abstained": 1,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 21,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 5,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 1,
                            "abstained": 1,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                }
            ]
        },
        {
            "id": 9,
            "name": "Bargeldobergrenze und Verbot anonymer Cryptozahlungen",
            "title": "Bargeldobergrenze",
            "description": "Bargeldgeschäfte sollen auf 10.000 EUR begrenzt und ab 3.000 EUR meldepflichtig werden. Zahlungen mit anonymen Kryptowallets sollen verboten werden.",
            "terms": [
                {
                    "term": "Bargeldgeschäfte ",
                    "explanation": "sind hierbei Geschäfte, bei denen gewerbliche Waren oder Dienstleistungen mit Bargeld bezahlt werden. Dies soll der Bekämpfung von Schwarzgeld und Terrorismusunterstützung dienen.",
                    "link": null
                },
                {
                    "term": "Meldepflicht ",
                    "explanation": "bedeutet hierbei dass Transaktionen ab 3.000 EUR an eine zentrale Behörde gemeldet werden müssen. Dabei müssen Käufer ihre Meldedaten angeben und erklären, woher das Bargeld stammt.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf den Seiten des EU-Parlaments",
                    "link": "https://www.europarl.europa.eu/doceo/document/PV-9-2024-04-24-ITM-015-22_DE.html",
                    "date": "24.04.2024"
                }
            ],
            "background": [
                {
                    "title": "Tagesschau: Obergrenze für Bargeldkäufe",
                    "link": "https://www.tagesschau.de/wirtschaft/finanzen/bargeld-geldwaesche-100.html",
                    "date": "18.01.2024"
                },
                {
                    "title": "Verbraucherzentralen: Bargeld als Zahlungsmittel erhalten",
                    "link": "https://www.vzbv.de/meldungen/bargeld-als-zahlungsmittel-erhalten",
                    "date": "09.03.2023"
                },
                {
                    "title": "Umfrage: Verbraucherinnen und Verbraucher wollen mit Bargeld bezahlen",
                    "link": "https://www.vzbv.de/pressemitteilungen/verbraucherinnen-wollen-mit-bargeld-bezahlen",
                    "date": "27.12.2021"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 12,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 4
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 6,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 3
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 20,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 18,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 5
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 5,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                }
            ]
        },
        {
            "id": 10,
            "name": "Recht auf Reparatur",
            "title": "Recht auf Reparatur",
            "description": "Die Reparatur von Geräten soll durch eine Reihe von Massnahmen gefördert werden. Hersteller werden verpflichtet, Anleitungen, Ersatzteile und Werkzeug verfügbar zu machen und Reparaturen als Teil der Gewährleistung anzubieten. Zudem werden reparaturfeindliche Praktiken (wie z.B. das \"Part Pairing\") verboten. Zu Beginn betroffen sind u.a. Smartphones und Tablets sowie Haushaltsgeräte wie Waschmaschinen, Kühlschränke oder Geschirrspüler. Mit der Zeit sollen weitere Produktgruppen hinzukommen.",
            "terms": [
                {
                    "term": "Part Pairing",
                    "explanation": "bezeichnet die Einschränkung des Betriebs auf konkrete Bauteile, indem ein Gerät (z.B.Smartphone) deren Seriennummern überprüft. Ein Ersatz dieser Komponenten ist dann ohne Zustimmung des Herstellers nicht möglich oder mit Einschränkungen des Funktionsumfangs verbunden. Diese Praxis steht in der Kritik, weil sie eigenständige Reparaturen und die Verwendung alternativer Ersatzteile verhindert. Typischerweise werden Sicherheitsargumente als Begründung angeführt.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf howtheyvote",
                    "link": "https://howtheyvote.eu/votes/168424",
                    "date": "23.04.2024"
                }
            ],
            "background": [
                {
                    "title": "Pressemitteilung von Runder Tisch Reparatur und Right to Repair Europe",
                    "link": "https://runder-tisch-reparatur.de/guter-anfang-aber-zu-wenig-produkte-eu-recht-auf-reparatur-nicht-ausreichend/",
                    "date": "23.04.2024"
                },
                {
                    "title": "Kommentar der Open Knowledge Foundation",
                    "link": "https://okfn.de/blog/2024/04/right-to-repair-entschieden-final/",
                    "date": "24.04.2024"
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 14,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 0,
                            "abstained": 9,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 4,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 20,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 23,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 2,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        },
        {
            "id": 11,
            "name": "Europäischer Gesundheitsdatenraum",
            "title": "Europäischer Gesundheitsdatenraum (EHDS)",
            "description": "Die Gesundheitsdaten aller Bürgerinnen und Bürger in der EU, sollen national zentral gespeichert und europaweit digital verfügbar gemacht werden. Für Behandlungszwecke und nur sehr eingeschränkt für Forschungszwecke und Statistik können Personen der Weitergabe ihrer Daten widersprechen.",
            "terms": [
                {
                    "term": "EHDS ",
                    "explanation": "steht für European Health Data Space, zu deutsch: Europäischer Gesundheitsdatenraum. Er umfasst die Speicherung der Gesundheitsdaten aller EU-Bürgerinnen und Bürger bei zentralen nationalen Behörden mit der Möglichkeit des europaweiten Abrufs dieser Daten durch berechtigte Personen und Stellen. Die Speicherung erfordert keine Einwilligung der Betroffenen. Sie können aber unter bestimmten Umständen einer Verarbeitung widersprechen. Für Zwecke des „öffentlichen Interesses“ ist der Widerspruch ausgeschlossen. Das impliziert Zwecke der Statistik, der politischen Gestaltung. Ebenfalls ausgeschlossen ist der Widerspruch bei Zwecken zum Schutz geistigen Eigentums und von Geschäftsgeheimnissen. Berechtigte Stellen können z.B. Krankenhäuser, Ärzt.innen, Versicherungen oder Pharma-Konzerne sein.",
                    "link": null
                },
                {
                    "term": "Gesundheitsdaten",
                    "explanation": "sind Daten aus Laboranalysen (genetische Tests, Blutwerte), aus Therapien (Medikationspläne, Nebenwirkungen) und allgemeine Informationen zum gesundheitlichen Zustand von Menschen (Krankheiten, Symptome). Für die Forschung sind sie vor allem in Verbindung mit Umfeld-Daten wie Ausbildung, Einkommen, Ernährung, Alkoholkonsum, Rauchen oder Bewegung interessant. Auch Umweltdaten wie Luft- oder Wasserqualität können hier eine wichtige Rolle spielen. Gesundheitsdaten werden von der Datenschutz-Grundverordnung besonders geschützt.",
                    "link": null
                }
            ],
            "howtheyvote": [
                {
                    "title": "Abstimmung auf howtheyvote",
                    "link": "https://howtheyvote.eu/votes/168444",
                    "date": "24.04.2024"
                }
            ],
            "background": [
                {
                    "title": "Kein effektiver Widerspruch gegen Nutzung von Gesundheitsdaten durch Dritte",
                    "link": "https://netzpolitik.org/2024/trilog-einigung-kein-effektiver-widerspruch-gegen-nutzung-von-gesundheitsdaten-durch-dritte/",
                    "date": "15.03.2024"
                },
                {
                    "title": "EHDS (Informationen der EU-Kommission)",
                    "link": "https://health.ec.europa.eu/ehealth-digital-health-and-care/european-health-data-space_de",
                    "date": null
                },
                {
                    "title": "Stellungnahme von Patentenrechte-Datenschutz",
                    "link": "https://patientenrechte-datenschutz.de/ehds-stellungnahme/",
                    "date": null
                }
            ],
            "answers": [
                {
                    "name": "spd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 13,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 3
                        }
                    }
                },
                {
                    "name": "volt",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "afd",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 7,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "freie",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 2,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "piraten",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "linke",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 5,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "csu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 5,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "gruene",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 7,
                            "against": 12,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 2
                        }
                    }
                },
                {
                    "name": "oedp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "cdu",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 23,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "fdp",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 4,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "independent",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 1,
                            "absent": 0,
                            "didntvote": 1
                        }
                    }
                },
                {
                    "name": "familien",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 1,
                            "against": 0,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                },
                {
                    "name": "partei",
                    "voting": {
                        "result": null,
                        "explanation": null,
                        "results": {
                            "for": 0,
                            "against": 1,
                            "abstained": 0,
                            "absent": 0,
                            "didntvote": 0
                        }
                    }
                }
            ]
        }
    ],
    "parties": [
        {
            "name": "cdu",
            "short_name": "CDU",
            "long_name": "Christlich Demokratische Union Deutschlands"
        },
        {
            "name": "gruene",
            "short_name": "Grüne",
            "long_name": "Bündnis 90/Die Grünen"
        },
        {
            "name": "spd",
            "short_name": "SPD",
            "long_name": "Sozialdemokratische Partei Deutschlands"
        },
        {
            "name": "afd",
            "short_name": "AfD",
            "long_name": "Alternative für Deutschland"
        },
        {
            "name": "csu",
            "short_name": "CSU",
            "long_name": "Christlich-Soziale Union in Bayern e.V."
        },
        {
            "name": "linke",
            "short_name": "Linke",
            "long_name": "DIE LINKE."
        },
        {
            "name": "fdp",
            "short_name": "FDP",
            "long_name": "Freie Demokratische Partei"
        },
        {
            "name": "partei",
            "short_name": "Die PARTEI",
            "long_name": "Die PARTEI"
        },
        {
            "name": "freie",
            "short_name": "Freie Wähler",
            "long_name": "Freie Wähler"
        },
        {
            "name": "oedp",
            "short_name": "ÖDP",
            "long_name": "Ökologisch-Demokratische Partei"
        },
        {
            "name": "familien",
            "short_name": "Familien",
            "long_name": "Familien-Partei Deutschlands"
        },
        {
            "name": "piraten",
            "short_name": "Piraten",
            "long_name": "Piratenpartei Deutschland"
        },
        {
            "name": "volt",
            "short_name": "Volt",
            "long_name": "Volt"
        },
        {
            "name": "independent",
            "short_name": "Parteilos",
            "long_name": "Independent"
        }
    ]
}