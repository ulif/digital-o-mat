var datenschutzPage = 
`<div id="privacy">
	<h2>Datenschutzerkl&auml;rung</h2>
	<p>
		Personenbezogene Daten werden auf dieser Webseite nur im technisch notwendigen Umfang erhoben. In keinem Fall werden
		die erhobenen Daten verkauft oder aus anderen Gr&uuml;nden an Dritte weitergegeben. Im Folgenden erhalten Sie einen
		&Uuml;berblick &uuml;ber die von uns im Rahmen dieser Website erhobenen Daten.
	</p>
	<h3>Keine Speicherung ihrer Auswahl und des Vergleichsergebnisses</h3>
	<p>
		Ihre inhaltliche Auswahl zu den Positionen im Digital-O-Maten und das Ergebnis Ihres Vergleichs werden nicht an den Server zurück übermittelt, vielmehr
		läuft der Digital-O-Mat einschließlich des Vergleichs vollständig im Arbeitsspeicher Ihres Endgeräts. Entsprechend
		sind für uns weder individuelle noch aggregierte Analysen der Nutzung des Digital-O-Maten möglich. Diese möglichst datensparsame 
		Umsetzung ist der Grund, warum wir technisch auf JavaScript zurückgreifen, welches entsprechend in Ihrem Web-Browser aktiviert sein muss, 
		um den Digital-O-Maten verwenden zu können. 
	</p>
	<h3>Nicht-personenbezogene Server-Logs</h3>
	<p>
		Bei Laden der Webseite des Digital-O-Maten werden zwangsläufig von Ihrem Web-Browser bestimmte Daten an den Server übermittelt. 
		Diese Daten werden vorübergehend in sogenannten	Log-Files gespeichert. Dies umfasst üblicherweise Informationen über den Typ und die Version des von Ihnen verwendeten
		Browsers, das verwendete Betriebssystem, die Referrer URL (die Webseite, von der aus Sie zu dieser Website gelangt
		sind), den Hostnamen des zugreifenden Rechners (die IP Adresse) sowie die Uhrzeit der Serveranfrage. In der Regel
		lassen sich diese Daten nicht bestimmten Personen – und damit auch nicht Ihnen – zuordnen. Auch wird weder 
		ein Abgleich der Log-Files mit anderen Daten vorgenommen, noch werden diese für irgendwelche Zwecke mit anderen Daten
		zusammengeführt. Die Log-Files werden regelmäßig nach einer statistischen Auswertung gelöscht.
	</p>
</div>`
