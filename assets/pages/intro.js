var introPage =
`<div id="intro">
	<div class="row">
		<h2>Digital-O-Mat</h2>
			<h3>Europawahl 2024</h3>
			<p>Welche Partei macht wirklich welche digitale Politik?</p>
			<p>
				Versprochen wird im Wahlkampf viel – am 9. Juni 2024 wollen deutsche Parteien ins Europaparlament gewählt werden. 
				<b>Der Digital-O-Mat zeigt auf einen Blick, wie die Parteien in der Vergangenheit tatsächlich abgestimmt haben</b> zur 
				Freiheit im Internet, zu Datenschutz, Überwachung und mehr.
				<!--Versprochen wird im Wahlkampf viel. Aber wie haben die Parteien, die am 26. Mai 2019 gewählt werden wollen, in der
				Vergangenheit zur Freiheit im Internet, zu Datenschutz und Überwachung tatsächlich abgestimmt? Der Digital-O-Mat
				gibt die Antwort!-->
			</p>
			<noscript>
				<h2>Bitte JavaScript aktivieren</h2>
				<p class="noscript-message">
					Der Digital-O-Mat benötigt JavaScript.
				</p>
			</noscript>
			<button href="/questionnaire" onclick="onNavItemClick('/questionnaire'); return false;" id="activate">Jetzt Abstimmungsverhalten vergleichen!</button>
			<p>
				Der Digital-O-Mat zur Europawahl 2024 lässt Sie auswählen, wie Sie bei zehn
				Schlüssel-Abstimmungen im Europäischen Parlament zwischen 2019 und 2024 abgestimmt hätten, und vergleicht dies mit
				dem tatsächlichen Abstimmungsverhalten der in Deutschland bei der Europawahl 2024 wählbaren Parteien.
			</p>
			<p>
                Der Digital-O-Mat zur Europawahl 2024 wird von der
                Digitalcourage Ortsgruppe Braunschweig zur Verfügung gestellt.
                Er basiert auf dem Digital-O-maten zur Europawahl 2019, der von
                einer Gruppe von NGOs, die sich mit Netzthemen befassen und die
                für	digitale Freiheitsrechte eintreten entwickelt wurde.
                Details zu uns und diesen NGOs siehe „Über uns“.  Zurück zu
                dieser Startseite gelangen Sie jederzeit per Klick auf das Logo
                oben links.
			</p>
            <h3>Updates und Änderungen</h3>
            <dl><dt><b>24.05.2024</b></dt><dd>BÖHMERMANN-SPEZIAL: Frage zu KI an europäischen Außengrenzen ergänzt</dd></dl>
            <dl><dt><b>23.05.2024</b></dt><dd>Frage zur KI-Verordnung durch eindeutigere Frage zu KI-gestützter Überwachung des öffentl. Raums ersetzt.</dd></dl>
		</div>
		<div class="row">
			<img class="image-margin" src="../assets/images/Digital-o-Mat_Europakarte_v04.svg" alt="Europakarte">
		</div>
	</div>
</div>`
