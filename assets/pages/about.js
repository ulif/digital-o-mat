
var aboutPage = 
`<div id="orgs">
	<h2>Über uns</h2>
    <p>
        Diese Version des Digital-O-Maten wurde von der Ortsgruppe Braunschweig im Digitalcourage e.V. gebaut.
    </p>
    <div class="org">
        <h3><a href="https://digitalcourage.de/braunschweig">Ortsgruppe Braunschweig</a></h3>
        <img alt="Logo OG Braunschweig" src="../assets/images/orgs/logo_og_bs_grau.svg">
        <p>
            Die Digitalcourage Ortsgruppe Braunschweig besteht aus
            Ehrenamtlichen, die sich in ihrer Freizeit um Datenschutz, digitale
            Grundrechte und Digitale Selbstverteidigung kümmern. Wir lieben
            Cookies und Einhörner. Und wir danken den Organisationen unten für
            ihre tolle Arbeit.
        </p>
        <p>
            <a href="https://digitalcourage.de/braunschweig" title="Link zur Ortsgruppe Braunschweig">https://digitalcourage.de/braunschweig/</a>
        </p>
    </div>
	<p>
            Er basiert technisch auf dem Digital-O-Maten zur Europawahl 2019,
            welcher von den folgenden fantastischen Organisationen und Gruppen
            als Kooperationsprojekt entwickelt wurde. Dort gibt es auch weitere
            Informationen über wichtige Entwicklungen rund um digitale Themen
            und darüber, wie man sich für digitale Grundrechte einsetzen kann:
	</p>
	<div class="org">
		<h3><a href="https://buendnis-freie-bildung.de/">Bündnis Freie Bildung</a></h3>
		<img alt="Logo Bündnis Freie Bildung" src="../assets/images/orgs/logo_bfb_grau.svg">
		<p>
			Das Bündnis Freie Bildung vereinigt Organisationen, Institutionen und
			Einzelpersonen, die sich für freie Bildung, frei zugängliche
			Bildungsmaterialien, offene Bildungspraktiken und offene Lizenzen in der
			Bildung einsetzen.
		</p>
		<p>
			<a href="https://buendnis-freie-bildung.de/" title="Link zu Bündnis Freie Bildung">https://buendnis-freie-bildung.de/</a>
		</p>
	</div>
	<div class="org">
		<h3><a href="https://ccc.de/">Chaos Computer Club</a></h3>
		<img alt="Logo Chaos Computer Club" src="../assets/images/orgs/logo_ccc_grau.svg">
		<p>
			Der Chaos Computer Club e. V. (CCC) ist die gr&ouml;&szlig;te europ&auml;ische Hackervereinigung und seit
			&uuml;ber drei&szlig;ig Jahren Vermittler im Spannungsfeld technischer und sozialer Entwicklungen. Die
			Aktivit&auml;ten des Clubs reichen von technischer Forschung und Erkundung am Rande des Technologieuniversums
			&uuml;ber <a href="https://www.ccc.de/de/campaigns">Kampagnen</a>, <a
				href="http://events.ccc.de/">Veranstaltungen</a>, Politikberatung, <a
				href="http://www.ccc.de/pressemitteilungen">Pressemitteilungen</a>&nbsp;und Publikationen bis zum Betrieb
			von Anonymisierungsdiensten und Kommunikationsmitteln.
		</p>
		<p>
			<a href="https://ccc.de/" title="Link zum Chaos Computer Club">https://ccc.de/</a>
		</p>
	</div>
	<div class="org">
		<h3><a href="https://digitalcourage.de/">digitalcourage e.V.</a></h3>
		<img alt="Logo digitalcourage e.V." src="../assets/images/orgs/logo_dc_grau.png">
		<p>
			Digitalcourage arbeitet für eine lebenswerte Welt im digitalen Zeitalter.
			
			Digitalcourage e.V. (vormals: FoeBuD e.V.) setzt sich seit 1987 für Grundrechte und Datenschutz ein. Bei Digitalcourage
			treffen unterschiedlichste Menschen zusammen, die Technik und Politik kritisch erkunden und menschenwürdig gestalten
			wollen.
			
			Wir wehren uns dagegen, dass unsere Demokratie „verdatet und verkauft“ wird. Wir wollen keine Gesellschaft, in
			der
			Menschen nur noch als Marketingobjekte, Manövriermasse beim Abbau des Sozialstaates oder als potentielle Terroristen
			behandelt werden. Wir wollen eine lebendige Demokratie.
			
			Digitalcourage klärt durch Öffentlichkeitsarbeit, Vorträge, Veranstaltungen und charmante Aktionen auf. So richtet
			Digitalcourage jährlich die BigBrotherAwards („Oscars für Datenkraken“) in Deutschland aus. Mit unserem
			Fachwissen
			mischen wir uns – auch ungefragt – in politische Prozesse ein.
		</p>
		<p>
			<a href="https://digitalcourage.de/" title="Link zu digitalcourage e.V.">https://digitalcourage.de/</a>
		</p>
	</div>
	<div class="org">
		<h3><a href="https://digitalegesellschaft.de/">Digitale Gesellschaft</a></h3>
		<img alt="Logo Digitale Gesellschaft" src="../assets/images/orgs/logo_digiges_grau.svg">
		<p>
			Digitale Gesellschaft e.V. wurde 2010 gegr&uuml;ndet. Die Idee ist, eine kampagnenorientierte Initiative f&uuml;r
			eine menschenrechts- und verbraucherfreundliche Netzpolitik zu schaffen. Wir wollen auf Erfahrungen aus anderen
			sozialen Bewegungen wie der Umweltbewegung aufbauen und diese digital weiterdenken. Auf nationaler und
			europ&auml;ischer Ebene gibt es zudem eine Vielzahl an Unternehmen und Verb&auml;nden, die f&uuml;r ihre jeweiligen
			Interessen werben. Was bisher allerdings fehlte, ist eine starke Nutzendenvertretung, die sich in aktuelle
			Gesetzgebungsprozesse einmischt, Stellungnahmen aus Nutzendensicht dazu verfasst und diese Interessen auch bei
			Anh&ouml;rungen und in Gespr&auml;chen mit Politikern nachdr&uuml;cklich vertritt. Daf&uuml;r gibt es nun uns.
		</p>
		<p>
			<a href="https://digitalegesellschaft.de/" title="Link zur Digitalen Gesellschaft">https://digitalegesellschaft.de/</a>
		</p>
	</div>
	<div class="org">
		<h3><a href="https://freifunk.net/">Freifunk</a></h3>
		<img alt="Logo Freifunk" src="../assets/images/orgs/logo_freifunk_grau.svg">
		<p>
			Die freifunk-Community ist Teil einer globalen Bewegung f&uuml;r freie Infrastrukturen und offene Funkfrequenzen.
			Unsere Vision ist die Demokratisierung der Kommunikationsmedien durch freie Netzwerke. Die praktische Umsetzung
			dieser Idee nehmen freifunk-Communities in der ganzen Welt in Angriff. Alle Nutzenden im freifunk-Netz stellen ihre
			WLAN-Router f&uuml;r den Datentransfer der anderen Teilnehmenden zur Verf&uuml;gung. Im Gegenzug k&ouml;nnen dann
			&uuml;ber die Router der anderen dann ebenfalls Daten wie Text, Musik, Filme usw. &uuml;bertragen oder von
			Teilnehmenden eingerichtete Dienste genutzt werden, zum Beispiel um zu chatten, zu telefonieren oder gemeinsam
			Onlinegames zu spielen. Daf&uuml;r nutzen wir <a href="https://freifunk.net/worum-geht-es/technik-der-community-netzwerke/">Mesh
				Netzwerke</a>. Viele stellen zudem ihren Internetzugang zur Verf&uuml;gung und erm&ouml;glichen anderen den Zugang
			zum weltweiten Netz.
		</p>
		<p>
			<a href="https://freifunk.net/" title="Link zu Freifunk">https://freifunk.net/</a>
		</p>
	</div>
	<div class="org">
		<h3><a href="https://okfn.de/">Open Knowledge Foundation Deutschland</a></h3>
		<img alt="Logo Open Knowledge Foundation Deutschland" src="../assets/images/orgs/logo_okfde_grau.svg">
		<p>
			Der Open Knowledge Foundation Deutschland e.V. ist der deutsche Zweig der Open Knowledge Foundation, eines <a href="https://okfn.org/network/">weltweit
				aktiven Netzwerks</a>&nbsp;von Aktivisten, Entwicklern, Policy-Spezialisten und Wissenschaftlern, die sich
			f&uuml;r die F&ouml;rderung offenen Wissens und offener Daten einsetzen. Wir verstehen uns als aktiver Teil der
			deutschen und europ&auml;ischen Zivilgesellschaft und f&ouml;rdern und st&auml;rken die Grundrechte. Besonders
			setzen wir uns daf&uuml;r ein, dass B&uuml;rgerinnen und B&uuml;rger ihre Rechte auch online wahrnehmen
			k&ouml;nnen. Freies Wissen hilft B&uuml;rgerinnen und B&uuml;rgern, sich zu informieren und f&uuml;hrt zu einer
			aufgekl&auml;rten Meinungsbildung.
		</p>
		<p>
			<a href="https://okfn.de/" title="Link zur Open Knowledge Foundation Deutschland">https://okfn.de/</a>
		</p>
	</div>
	<div class="org">
		<h3><a href="https://www.wauland.de/">Wau Holland Stiftung</a>
		</h3>
		<img alt="Logo Wau Holland Stiftung" src="../assets/images/orgs/whs_logo_gray.png">
		<p>
			Wau Holland hat mit der Gründung des Chaos-Computer-Clubs im Jahr 1981 den Weg der „Hacker“ in die Legalität und
			damit in die gesellschaftliche Verantwortung bereitet und mitbestimmt. Die Wau Holland Stiftung wurde vom Vater und engen Freunden des verstorbenen
			Datenphilosophen Wau Holland gegründet. Stiftungsziel ist es, die freidenkerischen Ansätze Wau Hollands zu bewahren und weiterzuführen.
		</p>
		<p>
			<a href="https://www.wauland.de/" title="Link zur Wau Holland Stiftung">https://www.wauland.de/</a>
		</p>
	</div>
	<div class="org">
		<h3><a href="https://www.whistleblower-net.de/">Whistleblower-Netzwerk e.V.</a>
		</h3>
		<img alt="Logo Whistleblower-Netzwerk e.V." src="../assets/images/orgs/logo_WBNW_grau.png">
		<p>
			Das Whistleblower-Netzwerk ist ein gemeinnütziger Verein mit derzeit ca. 100 Mitgliedern, der sich für
			Whistleblower (Hinweisgeber/Skandalaufdecker) einsetzt. Die Arbeit des Vereins wird bisher ausschließlich über
			ehrenamtlich Tätige geleistet. Das Whistleblower-Netzwerk berät und betreut bereits einige Whistleblower, recherchiert und
			veröffentlicht gegebenenfalls deren Fälle, setzt sich für die Betroffenen ein.
			Außerdem bietet das Netzwerk mit der Internetpräsenz <a
				href="https://www.whistleblower-net.de/">www.whistleblower-netzwerk.de</a> eine Plattform für Informationen
			rund
			um das Thema Whistleblower und Whistleblowing, sowohl über den Verein und seine Aktivitäten, als auch einen Blog
			mit tagesaktuellen Nachrichten.
		</p>
		<p>
			<a href="https://www.whistleblower-net.de/" title="Link zum Whistleblower-Netzwerk e.V.">https://www.whistleblower-net.de/</a>
		</p>
	</div>
	<div class="org">
		<h3><a href="https://wikimedia.de/">Wikimedia Deutschland e.V.</a></h3>
		<img alt="Logo Wikimedia Deutschland e.V." src="../assets/images/orgs/logo_wikimedia_grau.svg">
		<p>
			Wikimedia Deutschland ist ein gemeinn&uuml;tziger Verein, der sich der F&ouml;rderung <a href="https://de.wikipedia.org/wiki/Wissensallmende">Freien
				Wissens</a>&nbsp;verschrieben hat und den freien Zugang zu Wissen als Teil eines Grundrechts auf Bildung und
			Kulturteilhabe versteht. Daf&uuml;r gen&uuml;gt es nicht, Wissen nur konsumieren zu k&ouml;nnen. Menschen
			m&uuml;ssen auch selber Wissen beitragen und formen d&uuml;rfen. Seit unserer Gr&uuml;ndung im Jahre 2004 haben wir
			deshalb zahlreiche Wikimedia-Projekte unterst&uuml;tzt. Eines der wichtigsten ist die <a href="https://de.wikipedia.org/wiki/Wikipedia:Hauptseite">Wikipedia</a>,
			die freie Online-Enzyklop&auml;die. Ihre Inhalte und die aller anderen <a href="https://www.wikimedia.org/">Wikimedia-Projekte</a>&nbsp;werden
			von Freiwilligen erstellt, verbessert und verbreitet. Ein internationales Netzwerk aus Wikimedia-Organisationen
			hilft ihnen dabei.
		</p>
		<p>
			<a href="https://wikimedia.de/" title="Link zu Wikimedia">https://wikimedia.de/</a>
		</p>
	</div>
</div>`
