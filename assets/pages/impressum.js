var impressum = 
`<div id="imprint">
	<h2>Diensteanbieter</h2>
	<p>
        Ortsgruppe Braunschweig im Digitalcourage e.V.<br />
        c/o Stratum 0<br />
        Hamburger Str. 273A, Eingang A2<br />
        38114 Braunschweig
	</p>
	<p>
		E-Mail: <a href="mailto:braunschweig@digitalcourage.de">braunschweig@digitalcourage.de</a>
	</p>
	<p>
		Telefon: +49 (0)531-287 69 24-5
	</p>
	<h3>Haftung f&uuml;r Inhalte</h3>
	<p>
		Die Inhalte unserer Seiten wurden mit gr&ouml;&szlig;ter Sorgfalt erstellt. F&uuml;r die Richtigkeit,
		Vollst&auml;ndigkeit und Aktualit&auml;t der Inhalte k&ouml;nnen wir jedoch keine Gew&auml;hr &uuml;bernehmen. Als
		Diensteanbieter sind wir gem&auml;&szlig; &sect; 7 Abs.1 TMG f&uuml;r eigene Inhalte auf diesen Seiten nach den
		allgemeinen Gesetzen verantwortlich. Nach &sect;&sect; 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht
		verpflichtet, &uuml;bermittelte oder gespeicherte fremde Informationen zu &uuml;berwachen oder nach Umst&auml;nden
		zu forschen, die auf eine rechtswidrige T&auml;tigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der
		Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unber&uuml;hrt. Eine diesbez&uuml;gliche
		Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m&ouml;glich. Bei
		Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
	</p>
	<h3>Haftung f&uuml;r Links</h3>
	<p>
		Unser Angebot enth&auml;lt Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb
		k&ouml;nnen wir f&uuml;r diese fremden Inhalte auch keine Gew&auml;hr &uuml;bernehmen. F&uuml;r die Inhalte der
		verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten
		wurden zum Zeitpunkt der Verlinkung auf m&ouml;gliche Rechtsverst&ouml;&szlig;e &uuml;berpr&uuml;ft. Rechtswidrige
		Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.
	</p>
	<h3>Urheberrecht</h3>
	<p>
		Sofern nicht anders angegeben und mit Ausnahme der hier verwendeten Logos, stehen die Inhalte dieser Seite unter der
		Creative-Commons-Lizenz Namensnennung 4.0 International (<a href="https://creativecommons.org/licenses/by/4.0/legalcode">CC
		BY-SA 4.0</a>). Die Software des Digital-O-Maten wurde von <a href="https://dsst.io/">Data Science &amp;
		Stories</a> entwickelt, erst von Jennifer Gebske und dann von der Ortsgruppe Braunschweig weiterentwickelt. Die ursprüngliche Version ist unter anderem verf&uuml;gbar unter <a href="https://gitlab.com/jengeb/digital-o-mat/blob/master/LICENSE.md">MIT
		License</a> auf <a href="https://gitlab.com/jengeb/digital-o-mat">GitLab</a>. Die aktuelle Version ist auf <a href="https://codeberg.org/dcbs/digital-o-mat">Codeberg</a> verfügbar.
	</p>
	<p>
		„Digital-O-Mat-Logo“ von ElioQoshi <a href="http://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>
		via <a href="https://commons.wikimedia.org/wiki/File:Digitalomat_Logo.svg">Wikimedia Commons</a>;
		<a href="https://icomoon.io/">Icomoon Free</a> von <a href="http://keyamoon.com/">KeyaMoon</a> (<a href="https://creativecommons.org/licenses/by/4.0/">CC
			BY 4.0</a>);
		<a href="http://www.entypo.com/">Entypo+</a> von <a href="http://www.danielbruce.se/">Daniel Bruce</a> (<a href="https://creativecommons.org/licenses/by-sa/4.0/">CC
			BY-SA 4.0</a>);
		<a href="http://www.opensans.com/">Open Sans</a> (Apache License 2.0);
		<a href="https://github.com/impallari/Raleway">Raleway</a> (SIL OFL 1.1);
		<a href="http://zeptojs.com/">Zepto.js</a> (MIT License);
		<a href="https://github.com/janl/mustache.js">Mustache.js</a> (MIT License);
		<a href="https://www.chartjs.org/">Chart.js</a> (MIT License);
		<a href="https://github.com/emn178/chartjs-plugin-labels">chartjs-plugin-labels</a> (MIT License).
	</p>
</div>`
